#!/bin/sh

rm -rf $1/texmf/tex/latex/tuddesign/logo/*
cp src/fixed-a1b-graphics/tud_logo_a1b.pdf $1/texmf/tex/latex/tuddesign/logo/tud_logo.pdf
rm -rf $1/texmf/tex/latex/tuddesign/thesis/by-nc-nd*
cp src/fixed-a1b-graphics/by-nc-nd_a1b.pdf $1/texmf/tex/latex/tuddesign/thesis/by-nc-nd.pdf
