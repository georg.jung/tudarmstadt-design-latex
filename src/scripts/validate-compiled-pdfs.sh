#!/bin/sh

fail=false

for pdfile in *.pdf ; do
	curl -sS -X POST -F "File=@${pdfile}" https://www.pdf-online.com/osa/validate.aspx >${pdfile%.*}-validation.json
	if grep -F "The document does conform to the PDF/A-1b standard." "${pdfile%.*}-validation.json"
	then
		echo "${pdfile} validated successful"
	else
		fail=true
		1>&2 echo
		1>&2 echo "${pdfile} VALIDATION FAILED"
		1>&2 cat ${pdfile%.*}-validation.json
	fi
done
if [ "$fail" = true ] ; then
	1>&2 echo
	1>&2 echo
	1>&2 echo "Validation of at least one pdf failed. Exiting with 1"
	exit 1
fi
