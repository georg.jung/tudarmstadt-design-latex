#!/bin/sh

# This file should work based on its own location, not based on the working directory, as the repository
# it is contained in has a defined structure.
# By changing the working dir accordingly we can call this install file from whichever working directory
# we want, i.e. the repos base direcotry or the scripts directory this file resides in.

# get the repo's base dir
parent_path=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd -P)
parent_path=$parent_path/../..
cd "$parent_path"

# download old corporate design if none exists
sh src/scripts/helper/installstep-download.sh src/corporatedesign

. src/scripts/helper/installer.sh
