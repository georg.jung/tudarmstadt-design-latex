#!/bin/sh

# This file should work based on its own location, not based on the working directory, as the repository
# it is contained in has a defined structure.
# By changing the working dir accordingly we can call this install file from whichever working directory
# we want, i.e. the repos base direcotry or the scripts directory this file resides in.

# get the repo's base dir
parent_path=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd -P)
parent_path=$parent_path/../..
cd "$parent_path"

# Check TU Corporate Design exists
if [ ! ls src/corporatedesign/*.zip 1>/dev/null 2>&1;]
then
	1>&2 echo # blank line on stderr
	1>&2 echo "src/corporatedesign/ does not contain any zip files. You need to download the TU Darmstadt Corporate design and place it in that directory prior to installing."
	exit 1
fi

. src/scripts/helper/installer.sh
