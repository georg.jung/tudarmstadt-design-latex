Folgenden Code in der Präambel des Dokuments einfügen:


	% fix urls in mendeley-generated bib files
	% https://tex.stackexchange.com/a/309995/192065
	\DeclareSourcemap{
		\maps{
			\map{ % Replaces '{\_}', '{_}' or '\_' with just '_'
				\step[fieldsource=url,
					  match=\regexp{\{\\\_\}|\{\_\}|\\\_},
					  replace=\regexp{\_}]
			}
			\map{ % Replaces '{'$\sim$'}', '$\sim$' or '{~}' with just '~'
				\step[fieldsource=url,
					  match=\regexp{\{\$\\sim\$\}|\{\~\}|\$\\sim\$},
					  replace=\regexp{\~}]
			}
			% see https://tex.stackexchange.com/a/444275/192065
			\map{
				\step[fieldsource=url,
					  match=\regexp{\{\\\x{26}\}},
					  replace=\regexp{\x{26}}]
			}
		}
	}
	

Quelle und weitere Infos: <https://tex.stackexchange.com/a/309995/192065>